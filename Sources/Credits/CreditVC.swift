//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 04.06.2021.
//

import UIKit
import DSKit
import SnapKit
import KMPCore

class CreditVC: UIViewController {

    var apiClient: CreditApiClient?
    var accountStorage: AccountStorageProtocol?

    let stack = UIStackView()
    let nextPaymentLabel = UILabel()
    let debtLabel = UILabel()
    let accountLabel = UILabel()

    let nextPaymentText = "Ближайший платеж"
    let debtText = "Остаток долга"
    let accountText = "На счете для погашения"

    override func loadView() {
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 16
        [
            nextPaymentLabel,
            debtLabel,
            accountLabel,
        ].forEach {
            $0.numberOfLines = 0
            stack.addArrangedSubview($0)
        }

        let view = UIView()
        view.backgroundColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary)
        view.addSubview(stack)
        stack.snp.makeConstraints {
            $0.width.equalToSuperview().inset(48)
            $0.center.equalToSuperview()
        }
        self.view = view
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
    }

    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
        view.backgroundColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary)
        loadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Credit"
        loadData()
    }

    func loadData() {

        apiClient?.loadData { (credit) in
            self.setupData(credit: credit)
        }
        accountStorage?.getAccountSumm{ (value) in
            self.accountLabel.attributedText = self.configureString(title: self.accountText, value: "\n\(value) p")
        }
    }

    func setupData(credit: CreditModelProtocol) {
        title = credit.name + " кредит"
        let color = ThemeManager.shared.currentTheme.getColor(.textPrimary) ?? .black
        nextPaymentLabel.attributedText = credit
            .payment
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 22).font)
            .color(color)
        debtLabel.attributedText = credit
            .datePayment
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 16).font)
            .color(color)
    }

    func configureString(title: String, value: String) -> NSAttributedString {
        let string = NSMutableAttributedString()
        let paragraph = NSMutableParagraphStyle()
        let color = ThemeManager.shared.currentTheme.getColor(.textPrimary) ?? .black
        paragraph.alignment = .center
        string.append(NSAttributedString(
            string: title,
            attributes: [
                NSAttributedString.Key.foregroundColor : color,
                NSAttributedString.Key.font : UIFont.caption1,
                NSAttributedString.Key.paragraphStyle: paragraph,
            ]
        ))
        string.append(NSAttributedString(
            string: value,
            attributes: [
                NSAttributedString.Key.foregroundColor : color,
                NSAttributedString.Key.font : UIFont.body2,
                NSAttributedString.Key.paragraphStyle: paragraph,
            ]
        ))
        return string
    }
}
