//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import Foundation

struct CreditApiClient {
    var creditModel: CreditModelProtocol
    func loadData(completion: @escaping (CreditModelProtocol) -> Void) {
        completion(creditModel)
    }
}

struct CreditsDataProvider: CreditsDataProviderProtocol {
    func loadData(completion: @escaping ([CreditModelProtocol]) -> Void) {
        completion([
            CreditModel(name: "Потребительский", payment: "7 111 Р/мес", amount: " 123 222, 02 Р", datePayment: "След. платеж 14.08.2021"),
            CreditModel(name: "Большой", payment: "4 541 Р/мес", amount: " 90 112, 04 Р", datePayment: "След. платеж 16.06.2021"),
            CreditModel(name: "Обижающий", payment: "1 141 Р/мес", amount: " 542 019, 00 Р", datePayment: "След. платеж 11.09.2021"),
            CreditModel(name: "Невыносивый", payment: "5 152 Р/мес", amount: "200 000, 00 Р", datePayment: "След. платеж 29.10.2021"),
        ])
    }
}

