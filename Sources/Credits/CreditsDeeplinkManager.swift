//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import UIKit
import KMPCore

public struct CreditsDeeplinkManager: DeeplinkManagerProtocol {
    public func canResolve(url: URL) -> Bool {
        true
    }

    public func resolve(url: URL) {
        let vc = CreditFactory().makeCredit(
            creditModel: CreditModel(name: "Credit", payment: url.absoluteString, amount: "amount", datePayment: "date"),
            accountStorage: nil)
        deeplinkProvider?.deeplinkCoordinator?.present(controller: vc)
    }

    weak var deeplinkProvider: DeeplinkCoordinatorContainable?
    public init(deeplinkProvider: DeeplinkCoordinatorContainable) {
        self.deeplinkProvider = deeplinkProvider
    }

}
