//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import Foundation

public struct CreditModel: CreditModelProtocol {
    public var name: String
    public var payment: String
    public var amount: String
    public var datePayment: String
    public init(
        name: String,
        payment: String,
        amount: String,
        datePayment: String
    ) {
        self.name = name
        self.payment = payment
        self.amount = amount
        self.datePayment = datePayment
    }
}
