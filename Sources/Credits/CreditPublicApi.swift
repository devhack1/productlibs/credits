//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit

public protocol AccountStorageProtocol {
    func getAccountSumm(completion: @escaping (Double) -> Void)
}

public protocol CreditModelProtocol {
    var name: String { get }
    var payment: String { get }
    var amount: String { get }
    var datePayment: String { get }
}

public struct CreditFactory {
    public init() {}
    public func makeCredit(
        creditModel: CreditModelProtocol,
        accountStorage: AccountStorageProtocol?
    ) -> UIViewController {
        let vc = CreditVC()
        vc.accountStorage = accountStorage
        vc.apiClient = CreditApiClient(creditModel: creditModel)
        return vc
    }
}

public struct CreditsDataProviderFactory {
    public init() {}
    public func makeProvider() -> CreditsDataProviderProtocol {
        return CreditsDataProvider()
    }
}

public protocol CreditsDataProviderProtocol {
    func loadData(completion: @escaping ([CreditModelProtocol]) -> Void)
}
