import XCTest

import CreditsTests

var tests = [XCTestCaseEntry]()
tests += CreditsTests.allTests()
XCTMain(tests)
